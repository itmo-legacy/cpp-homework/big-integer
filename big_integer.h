#ifndef BIG_INTEGER_H
#define BIG_INTEGER_H

#include <string>
#include <vector>
#include <cstdint>
#include <limits>

struct big_integer {
    big_integer();
    big_integer(big_integer const &other) = default;  // TODO: should I put '= default' or just not write?
    big_integer(int a);
    explicit big_integer(std::string const &str);
    ~big_integer() = default;

    bool is_zero() const;

    big_integer &operator=(big_integer const &other) = default;
    big_integer &operator+=(big_integer const &rhs);
    big_integer &operator-=(big_integer const &rhs);
    big_integer &operator*=(big_integer const &rhs);
    big_integer &operator/=(big_integer const &rhs);
    big_integer &operator%=(big_integer const &rhs);
    big_integer &operator&=(big_integer const &rhs);
    big_integer &operator|=(big_integer const &rhs);
    big_integer &operator^=(big_integer const &rhs);
    big_integer &operator<<=(int rhs);
    big_integer &operator>>=(int rhs);

    big_integer operator+() const;
    big_integer operator-() const;
    big_integer operator~() const;
    big_integer &operator++();
    big_integer operator++(int);
    big_integer &operator--();
    big_integer operator--(int);

    friend bool operator==(big_integer const &a, big_integer const &b);
    friend bool operator!=(big_integer const &a, big_integer const &b);
    friend bool operator<(big_integer const &a, big_integer const &b);
    friend bool operator>(big_integer const &a, big_integer const &b);
    friend bool operator<=(big_integer const &a, big_integer const &b);
    friend bool operator>=(big_integer const &a, big_integer const &b);
    friend std::string to_string(big_integer const &a);
    friend big_integer divmod(big_integer &dividend, big_integer divisor);

private:
    using chunk_t = std::uint_fast32_t;
    using magnitude_t = std::vector<chunk_t>;

private:
    explicit big_integer(chunk_t chunk, int signum = 1);
    explicit big_integer(magnitude_t &magnitude, int signum);

    big_integer &negate();
    big_integer &invert();

    std::size_t size() const;

    template<typename F>
    big_integer &logical_operation(const big_integer &rhs, F const &functor);

    big_integer::chunk_t get_chunk(std::size_t i, std::size_t first_nonzero) const;
    std::size_t first_nonzero_index() const;
    void construct_from_two_complement();

private:
    constexpr static int CHUNK_SIZE = std::numeric_limits<chunk_t>::digits;
    static constexpr auto DIGITS_PER_CHUNK = (chunk_t) std::numeric_limits<chunk_t>::digits10; // TODO: should I use static_cast here
    static const big_integer DECIMAL_PER_CHUNK;

    int signum;
    magnitude_t magnitude;

private:
    friend void add_magnitude(magnitude_t &one, const magnitude_t &two);
    friend void subtract_magnitude(magnitude_t &one, const magnitude_t &two);
    friend void subtract_magnitude(big_integer::magnitude_t &result,
                                   const big_integer::magnitude_t &one,
                                   const big_integer::magnitude_t &two);
    friend void multiply_by_chunk(magnitude_t &one, const chunk_t &two);
    friend chunk_t divide_by_chunk(magnitude_t &one, const chunk_t &two);
    friend void increment(magnitude_t &magnitude);
    friend void decrement(magnitude_t &magnitude);
    friend int compare_magnitude(const magnitude_t &one, const magnitude_t &two);
    friend void strip_zeroes(magnitude_t &magnitude);
    // for long division
    friend magnitude_t divide_magnitude(magnitude_t &dividend, magnitude_t divisor);
    friend chunk_t estimate(const magnitude_t &n, const magnitude_t &d, std::size_t shift);
    friend int shifted_compare_magnitude(const magnitude_t &one, const magnitude_t &two, std::size_t shift);
    friend void shifted_subtract_magnitude(magnitude_t &one, const magnitude_t &two, std::size_t shift);
    friend void shifted_subtract_magnitude(magnitude_t &result,
                                           const magnitude_t &one,
                                           const magnitude_t &two,
                                           std::size_t shift);
    friend void normalize(magnitude_t &n, magnitude_t &d);
};

big_integer operator+(big_integer a, big_integer const &b);
big_integer operator-(big_integer a, big_integer const &b);
big_integer operator*(big_integer a, big_integer const &b);
big_integer operator/(big_integer a, big_integer const &b);
big_integer operator%(big_integer a, big_integer const &b);
big_integer operator&(big_integer a, big_integer const &b);
big_integer operator|(big_integer a, big_integer const &b);
big_integer operator^(big_integer a, big_integer const &b);
big_integer operator<<(big_integer a, int b);
big_integer operator>>(big_integer a, int b);

std::ostream &operator<<(std::ostream &s, big_integer const &a);


#endif // BIG_INTEGER_H
