#ifndef BIG_INTEGER_UTILS_H
#define BIG_INTEGER_UTILS_H

#include <type_traits>

// TODO: should I use separate namespace?
// TODO: how do I make this functions visible only for the project?

template<typename T>
constexpr T
high_bits(T x, int number = std::numeric_limits<T>::digits / 2) {       // should I just take copies?
    return x >> (std::numeric_limits<T>::digits - number);
}

template<typename T>
constexpr T
low_bits(T x, int number = std::numeric_limits<T>::digits / 2) {
    T low_mask = ((T) 1 << number) - 1;  // does it make sense to make it constexpr
    return x & low_mask;
}

template<typename T>
constexpr std::make_unsigned_t<T>
absolute_value(const T &n) {
    if (n < 0) {
        return std::make_unsigned_t<T>(-(n + 1)) + 1;
    } else {
        return n;
    }
}

template<typename T>
constexpr typename std::enable_if_t<std::is_unsigned<T>::value, T>
add_with_carry(T &one, T two, T carry = 0) {
    return (one += (two + carry)) < two + carry;
}

template<typename T>
constexpr typename std::enable_if_t<std::is_unsigned<T>::value, T>
subtract_with_carry(T &one, T two, T borrow = 0) {
    T temp = one;
    return (one -= (two + borrow)) + borrow > temp;
}

template<typename T>
constexpr typename std::enable_if_t<std::is_unsigned<T>::value and not std::is_same<T, std::uint32_t>::value, T>
multiply_with_carry(T &one, const T &two) {
#ifdef USE_UINT128
    __uint128_t result = (__uint128_t) one * two;
    one = low_bits(result, std::numeric_limits<T>::digits);
    return result >> std::numeric_limits<T>::digits;
#else
    T rl, rh, cl, ch;

    T x = low_bits(one) * low_bits(two);
    rl = low_bits(x);
    rh = high_bits(x);

    x = low_bits(one) * high_bits(two) + rh;
    rh = low_bits(x);
    cl = high_bits(x);

    x = high_bits(one) * low_bits(two) + rh;
    rh = low_bits(x);

    x = cl + high_bits(one) * high_bits(two) + high_bits(x);
    cl = low_bits(x);
    ch = high_bits(x);

    one = (rh << (std::numeric_limits<T>::digits / 2)) | rl;
    return (ch << (std::numeric_limits<T>::digits / 2)) | cl;
#endif
}

template<typename T>
constexpr typename std::enable_if_t<std::is_same<T, std::uint32_t>::value, T>
multiply_with_carry(T &one, const T &two) {
    std::uint64_t result = (std::uint64_t) one * two;
    one = (T) result;       // is it guaranteed to take low 32 bits?
    return result >> 32;
}

/**
 * UB if result doesn't fit into the used type
 * @tparam T unsigned integer type
 * @param high high bits of dividend, remainder after evaluation
 * @param low low bits of dividend, quotient after evaluation
 * @param divisor divisor
 */
template<typename T>
constexpr typename std::enable_if_t<(std::is_unsigned<T>::value and not std::is_same<T, uint32_t>::value), void>
divide_with_carry(T &high, T &low, const T &divisor) {
#ifdef USE_UINT128
    __uint128_t dividend = ((__uint128_t) high << std::numeric_limits<T>::digits) | low;
    high = dividend % divisor;
    low = dividend / divisor;
#else
    if (divisor == 1) { return; }

    T q1 = low / divisor;
    T remainder = low % divisor;

//    T quotient_high = high / divisor;
    T high_remainder = high % divisor;

    T quotient_per_one = std::numeric_limits<T>::max() / divisor;
    T remainder_per_one = std::numeric_limits<T>::max() % divisor;
    if (remainder_per_one + 1 == divisor) {
        ++quotient_per_one;
        remainder_per_one = 0;
    } else {
        ++remainder_per_one;
    }

    T additional = high_remainder;
    T carry = multiply_with_carry(additional, remainder_per_one);
    T q2, add_rem;
    if (carry == 0) {
        q2 = additional / divisor;
        add_rem = additional % divisor;
    } else {
        divide_with_carry(add_rem = carry, q2 = additional, divisor);
    };
    carry = add_with_carry(remainder, add_rem);
    T final_remainder, q3;
    if (carry == 0) {
        q3 = remainder / divisor;
        final_remainder = remainder % divisor;
    } else {
        divide_with_carry(final_remainder = carry, q3 = remainder, divisor);
    }
    low = q1
          + high_remainder * quotient_per_one
          + q2
          + q3;
    high = final_remainder;
#endif
}

template<typename T>
std::enable_if_t<std::is_same<T, uint32_t>::value, void>
divide_with_carry(T &high, T &low, const T &divisor) {
    std::uint64_t dividend = ((std::uint64_t) high << 32U) | low;
    high = dividend % divisor;
    low = dividend / divisor;
}

template<typename T>
constexpr typename std::enable_if_t<std::is_unsigned<T>::value, T>
shift_right_with_carry(T &one, int shift, const T &carry = 0) {                 // shift < 64
    T result = high_bits(one, shift);
    one = (one << shift) | carry;
    return result;
}

template<typename T>
constexpr typename std::enable_if_t<std::is_unsigned<T>::value, T>
shift_left_with_carry(T &one, int shift, const T &carry = 0) {
    T result = low_bits(one, shift);
    one = (carry << (std::numeric_limits<T>::digits - shift)) | (one >> shift);
    return result;
}

template<typename T>
constexpr int
compare(const T &one, const T &two) {
    if (one == two) {
        return 0;
    }
    return one < two ? -1 : 1;
}

template<typename T>
constexpr T
pow10(T exponent) {
    if (exponent == 0) {
        return 1;
    } else if (exponent % 2 == 0) {
        T result = pow10(exponent / 2);
        return result * result;
    } else {
        return 10 * pow10(exponent - 1);
    }
}


void add_magnitude(big_integer::magnitude_t &one, const big_integer::magnitude_t &two);

void subtract_magnitude(big_integer::magnitude_t &one, const big_integer::magnitude_t &two);

void subtract_magnitude(big_integer::magnitude_t &result,
                        const big_integer::magnitude_t &one,
                        const big_integer::magnitude_t &two);

void multiply_by_chunk(big_integer::magnitude_t &one, const big_integer::chunk_t &two);

/**
 * divides magnitude by one chunk
 * @param dividend magnitude to divide
 * @param divisor chunk
 * @return remainder of division
 */
big_integer::chunk_t divide_by_chunk(big_integer::magnitude_t &one, const big_integer::chunk_t &two);

void increment(big_integer::magnitude_t &magnitude);

void decrement(big_integer::magnitude_t &magnitude);

int compare_magnitude(const big_integer::magnitude_t &one, const big_integer::magnitude_t &two);

void strip_zeroes(big_integer::magnitude_t &magnitude);

big_integer::magnitude_t divide_magnitude(big_integer::magnitude_t &dividend, big_integer::magnitude_t divisor);

void normalize(big_integer::magnitude_t &n, big_integer::magnitude_t &d);

big_integer::chunk_t estimate(const big_integer::magnitude_t &n, const big_integer::magnitude_t &d, std::size_t shift);

int shifted_compare_magnitude(const big_integer::magnitude_t &one, const big_integer::magnitude_t &two,
                              std::size_t shift);

void shifted_subtract_magnitude(big_integer::magnitude_t &one, const big_integer::magnitude_t &two, std::size_t shift);

void shifted_subtract_magnitude(big_integer::magnitude_t &result,
                                const big_integer::magnitude_t &one,
                                const big_integer::magnitude_t &two,
                                std::size_t shift);

#endif //BIG_INTEGER_UTILS_H
