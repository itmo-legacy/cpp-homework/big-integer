#include "big_integer.h"

#include <map>
#include <sstream>
#include <algorithm>
#include <numeric>
#include "utils.h"

const big_integer big_integer::DECIMAL_PER_CHUNK{(chunk_t) pow10(DIGITS_PER_CHUNK)};

big_integer::big_integer() : signum(0), magnitude{} {}

big_integer::big_integer(int a) {
    if (a == 0) {
        signum = 0;
    } else {
        signum = a < 0 ? -1 : 1;
        unsigned num = absolute_value(a);
        if (CHUNK_SIZE >= std::numeric_limits<unsigned>::digits) {
            magnitude = {(chunk_t) num};
        } else {
            while (num != 0) {
                magnitude.push_back((chunk_t) low_bits(num, CHUNK_SIZE));
                num >>= CHUNK_SIZE;
            }
        }
    }
}

big_integer::big_integer(std::string const &str) {
    *this = 0;
    if (str.empty()) {
        return;
    }

    std::size_t cursor = 0;
    bool has_minus = false;
    if (str[0] == '-') {
        has_minus = true;
        cursor = 1;
    } else if (str[0] == '+') {
        cursor = 1;
    }

    std::size_t first_group_size = (str.length() - cursor) % DIGITS_PER_CHUNK;
    if (first_group_size == 0) {
        first_group_size = DIGITS_PER_CHUNK;
    }
    std::istringstream chunk_stream(str.substr(cursor, first_group_size));
    chunk_t chunk;
    chunk_stream >> chunk;
    *this += big_integer(chunk);
    cursor += first_group_size;

    for (; cursor < str.length(); cursor += DIGITS_PER_CHUNK) {
        chunk_stream = std::istringstream(str.substr(cursor, DIGITS_PER_CHUNK));
        chunk_stream >> chunk;
        *this *= DECIMAL_PER_CHUNK;
        *this += big_integer(chunk);
    }
    if (has_minus and not is_zero()) {
        negate();
    }
}

bool big_integer::is_zero() const {
    return signum == 0; // magnitude should always be empty if signum is 0
}

big_integer &big_integer::operator+=(big_integer const &rhs) {
    if (rhs.is_zero()) {
        return *this;
    } else if (this->is_zero()) {
        return *this = rhs;
    }
    if (signum == rhs.signum) {
        add_magnitude(magnitude, rhs.magnitude);
    } else {
        negate();
        *this -= rhs;
        negate();
    }
    return *this;
}

big_integer &big_integer::operator-=(big_integer const &rhs) {
    if (this->is_zero()) {
        return *this = -rhs;
    } else if (rhs.is_zero()) {
        return *this;
    }
    if (signum == rhs.signum) {
        const auto cmp = compare_magnitude(magnitude, rhs.magnitude);
        if (cmp == 0) {
            *this = 0;                                        // result is zero, probably should have used constant
        } else if (cmp > 0) {
            subtract_magnitude(magnitude, rhs.magnitude);
        } else {
            subtract_magnitude(magnitude, rhs.magnitude, magnitude);
            negate();
        }
    } else {
        negate();
        *this += rhs;
        negate();
    }
    return *this;
}

big_integer &big_integer::operator*=(big_integer const &rhs) {
    if (this->is_zero()) { return *this; }
    if (rhs.is_zero()) { return *this = rhs; }
    big_integer result = 0;
    int correct_signum = signum * rhs.signum;
    if (signum < 0) {
        negate();
    }
    for (std::size_t i = 0; i < rhs.size(); ++i) {
        big_integer lhs = *this;
        multiply_by_chunk(lhs.magnitude, rhs.magnitude[i]);
        lhs <<= i * CHUNK_SIZE;
        result += lhs;
    }
    result.signum = correct_signum;
    return *this = result;
}

big_integer &big_integer::operator/=(big_integer const &rhs) {
    divmod(*this, rhs);
    return *this;
}

big_integer &big_integer::operator%=(big_integer const &rhs) {
    return *this = divmod(*this, rhs); // TODO: more efficient implementation
}

big_integer &big_integer::operator&=(big_integer const &rhs) {
    return logical_operation(rhs, std::bit_and<>());
}

big_integer &big_integer::operator|=(big_integer const &rhs) {
    return logical_operation(rhs, std::bit_or<>());
}

big_integer &big_integer::operator^=(big_integer const &rhs) {
    return logical_operation(rhs, std::bit_xor<>());
}

big_integer &big_integer::operator<<=(int rhs) {                    // rhs should be > zero
    if (this->is_zero()) { return *this; }                          // TODO: reverse magnitude order
    auto chunks_count = (std::size_t) (rhs / CHUNK_SIZE);
    int shift_per_chunk = rhs % CHUNK_SIZE;
    if (shift_per_chunk != 0) {
        chunk_t carry = 0;
        for (auto &chunk: magnitude) {
            carry = shift_right_with_carry(chunk, shift_per_chunk, carry);
        }
        if (carry != 0) {
            magnitude.push_back(carry);
        }
    }
    magnitude.insert(magnitude.begin(), chunks_count, 0);
    return *this;
}

big_integer &big_integer::operator>>=(int rhs) {
    if (this->is_zero()) { return *this; }
    auto chunks_count = (std::size_t) (rhs / CHUNK_SIZE);
    int shift_per_chunk = rhs % CHUNK_SIZE;
    if (chunks_count >= size()) {
        return *this = 0;
    }
    bool lost_ones = false;
    if (signum < 0) {
        bool lost_nonzero_chunks = std::any_of(magnitude.begin(), std::next(magnitude.begin(), chunks_count),
                                               [](auto item) { return item != 0; });
        bool lost_shift_value = (size() >= chunks_count + 1)
                                or low_bits(magnitude[chunks_count], shift_per_chunk) != 0;
        lost_ones = lost_nonzero_chunks or lost_shift_value;
    }
    magnitude.erase(magnitude.begin(), std::next(magnitude.begin(), chunks_count));
    if (shift_per_chunk != 0) {
        chunk_t carry = 0;
        for (auto chunk_it = magnitude.rbegin(); chunk_it != magnitude.rend(); ++chunk_it) {
            carry = shift_left_with_carry(*chunk_it, shift_per_chunk, carry);
        }
    }
    if (lost_ones) {
        --*this;
    }
    strip_zeroes(magnitude);
    return *this;
}

big_integer big_integer::operator+() const {
    return *this;
}

big_integer big_integer::operator-() const {
    return big_integer(*this).negate();
}

big_integer big_integer::operator~() const {
    return big_integer(*this).invert();
}

big_integer &big_integer::operator++() {
    if (signum == 0) {
        return *this = 1;
    }
    if (signum == -1) {
        negate();
        --*this;
        negate();
        return *this;
    }
    increment(magnitude);
    return *this;
}

big_integer big_integer::operator++(int) {
    big_integer result(*this);
    ++*this;
    return result;
}

big_integer &big_integer::operator--() {
    if (*this == 1) {
        return *this = 0;
    }
    if (signum == -1) {
        negate();
        ++*this;
        negate();
        return *this;
    }
    decrement(magnitude);
    return *this;
}

big_integer big_integer::operator--(int) {
    big_integer result(*this);
    ++*this;
    return result;
}

bool operator==(big_integer const &a, big_integer const &b) {
    return a.signum == b.signum and a.magnitude == b.magnitude;
}

bool operator!=(big_integer const &a, big_integer const &b) {
    return not(a == b);
}

bool operator<(big_integer const &a, big_integer const &b) {
    if (a.signum < b.signum) {
        return true;
    }
    if (a.signum > b.signum) {
        return false;
    }
    int compare = compare_magnitude(a.magnitude, b.magnitude);
    compare *= a.signum;
    return compare < 0;
}

bool operator>(big_integer const &a, big_integer const &b) {
    return not(a <= b);
}

bool operator<=(big_integer const &a, big_integer const &b) {
    return a < b or a == b;
}

bool operator>=(big_integer const &a, big_integer const &b) {
    return not(a < b);
}

std::string to_string(big_integer const &a) {
    if (a.is_zero()) {
        return "0";
    }
    if (a.size() == 1) {
        return (a.signum < 0 ? "-" : "") + std::to_string(a.magnitude[0]);
    }
    std::vector<std::string> pieces;

    big_integer quotient(a);
    if (quotient.signum < 0) {
        quotient.negate();
    }
    big_integer remainder;
    while (quotient.size() != 0) {
        remainder = divmod(quotient, big_integer::DECIMAL_PER_CHUNK);
        std::string piece = to_string(remainder);
        if (quotient.size() != 0) {
            piece = std::string(big_integer::DIGITS_PER_CHUNK - piece.length(), '0').append(piece);
        }
        pieces.push_back(piece);
    }
    if (a.signum < 0) {
        pieces.emplace_back("-");
    }
    return std::accumulate(pieces.rbegin(), pieces.rend(), std::string(), std::plus<>());
}

big_integer operator+(big_integer a, big_integer const &b) {
    return a += b;
}

big_integer operator-(big_integer a, big_integer const &b) {
    return a -= b;
}

big_integer operator*(big_integer a, big_integer const &b) {
    return a *= b;
}

big_integer operator/(big_integer a, big_integer const &b) {
    return a /= b;
}

big_integer operator%(big_integer a, big_integer const &b) {
    return a %= b;
}

big_integer operator&(big_integer a, big_integer const &b) {
    return a &= b;
}

big_integer operator|(big_integer a, big_integer const &b) {
    return a |= b;
}

big_integer operator^(big_integer a, big_integer const &b) {
    return a ^= b;
}

big_integer operator>>(big_integer a, int b) {
    return a >>= b;
}

big_integer operator<<(big_integer a, int b) {
    return a <<= b;
}

std::ostream &operator<<(std::ostream &s, big_integer const &a) {
    return s << to_string(a);
}

big_integer divmod(big_integer &dividend, big_integer divisor) {
    if (compare_magnitude(dividend.magnitude, divisor.magnitude) < 0) {
        big_integer remainder = 0;
        std::swap(remainder, dividend);
        return remainder;
    }

    int initial_dividend_signum = dividend.signum;
    big_integer::magnitude_t remainder_m;
    dividend.signum *= divisor.signum;
    if (divisor.size() == 1) {
        remainder_m = {divide_by_chunk(dividend.magnitude, divisor.magnitude[0])};
        strip_zeroes(remainder_m);
    } else {
        remainder_m = divide_magnitude(dividend.magnitude, divisor.magnitude);
    }
    strip_zeroes(dividend.magnitude);
    return big_integer(remainder_m, initial_dividend_signum);
}

big_integer::big_integer(chunk_t chunk, int signum) {
    if (chunk == 0 or signum == 0) {
        this->signum = 0;
    } else {
        this->signum = signum;
        magnitude = {chunk};
    }
}

big_integer::big_integer(big_integer::magnitude_t &magnitude, int signum) {
    if (magnitude.empty() or signum == 0) {
        this->signum = 0;
    } else {
        this->signum = signum;
        this->magnitude = magnitude;
    }
}

big_integer &big_integer::negate() {
    signum = -signum;
    return *this;
}

big_integer &big_integer::invert() {
    negate();
    return --*this;
}

size_t big_integer::size() const {
    return magnitude.size();
}

template<typename F>
big_integer &big_integer::logical_operation(big_integer const &rhs, F const &functor) {
    if (this->is_zero()) { return *this = rhs; }
    if (rhs.is_zero()) { return *this; }
    magnitude.resize(std::max(size(), rhs.size()) + 1);
    auto this_first_nonzero = first_nonzero_index();
    auto rhs_first_nonzero = rhs.first_nonzero_index();
    for (std::size_t i = 0; i < size(); ++i) {
        magnitude[i] = functor(get_chunk(i, this_first_nonzero), rhs.get_chunk(i, rhs_first_nonzero));
    }
    construct_from_two_complement();
    return *this;
}

big_integer::chunk_t big_integer::get_chunk(std::size_t i, std::size_t first_nonzero) const {
    if (i >= size()) {
        return signum < 0 ? std::numeric_limits<chunk_t>::max() : 0;
    }
    if (signum >= 0) {
        return magnitude[i];
    } else {
        return i > first_nonzero ? ~magnitude[i] : -magnitude[i];
    }
}

std::size_t big_integer::first_nonzero_index() const {
    std::size_t i = 0;
    while (i < size() and magnitude[i] == 0) {
        ++i;
    }
    return i;
}

void big_integer::construct_from_two_complement() {
    if (high_bits(magnitude.back(), 1) == 0) {
        strip_zeroes(magnitude);
        signum = magnitude.empty() ? 0 : 1;
    } else {
        std::transform(magnitude.begin(), magnitude.end(), magnitude.begin(), std::bit_not<>());
        increment(magnitude);
        strip_zeroes(magnitude);
        signum = -1;
    }
}

